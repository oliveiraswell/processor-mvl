library	ieee;
use ieee.std_logic_1164.all;  
use ieee.std_logic_arith.all;			   
use ieee.std_logic_unsigned.all;
use work.constant_lib.all;
use work.mvl.all;

entity TB_MP is
end TB_MP;

architecture behv of TB_MP is

	component microprocessor is
		port( 	
			clk		:	in std_logic;
			rst		:	in qbit
		);
	end component;

	--Signals
	signal TB_clk: std_logic;
	signal TB_rst: qbit;

begin  			
	
	Unit: microprocessor port map(TB_clk, TB_rst);
	
	process
	begin
		TB_clk <= '0';			-- offer clock signal
		wait for 5 ns;			-- in cocurrent process
		TB_clk <= '1';
		wait for 5 ns;
	end process;
	
	process
	begin 
		TB_rst <= 3;
		wait for 50 ns;
		TB_rst <= 0;
		wait for 100000 ns;
	end process;	

end behv;				 

--------------------------------------------------------
configuration CFG_TB_MP of TB_MP is
	for behv
	end for;
end CFG_TB_MP;
--------------------------------------------------------