library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.mvl.all;
---------------------------------------------------
entity reg_four is
	port(	
		I 		:	in qbit_vector(3 downto 0);
		clock	:	in std_logic;
		clear	:	in qbit;
		Q 		:	out qbit_vector(3 downto 0)
	);
end reg_four;
----------------------------------------------------
architecture behv of reg_four is
	signal Q_tmp: qbit_vector(3 downto 0);
begin

	process(I, clock, clear)
	begin

	if clear = 3 then
			-- use 'range in signal assigment 
			Q_tmp <= (Q_tmp'range => 0);
	elsif (clock='1' and clock'event) then
		Q_tmp <= I;
	end if;

	end process;
	Q <= Q_tmp;

end behv;
