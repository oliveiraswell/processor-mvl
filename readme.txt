SCRIPT para execução
------------------

No modelSim
------------------

quit -sim
project compileall
vsim -gui work.microprocessor
add wave -position insertpoint  \
sim:/microprocessor/clk \
sim:/microprocessor/rst
add wave -position insertpoint sim:/microprocessor/iu/*
add wave -position insertpoint sim:/microprocessor/U1/*
add wave -position insertpoint sim:/microprocessor/U2/*
add wave -position insertpoint sim:/microprocessor/U3/*
add wave -position insertpoint sim:/microprocessor/U3/ao/*
add wave -position insertpoint sim:/microprocessor/U4/*
add wave -position insertpoint  \
sim:/microprocessor/U2/fr/tmp_rf
add wave -position insertpoint  \
sim:/microprocessor/U4/mem/tmp_ram
add wave -position insertpoint sim:/microprocessor/U1/mem/*
force -freeze sim:/microprocessor/clk_in 1 0, 0 {50 ps} -r 100
force -freeze sim:/microprocessor/rst_in 3 0 -cancel 300
run -all
quit -sim