library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.mvl.all;
---------------------------------------------------
entity reg is

	port(
		I 			   :	in  qbit_vector(40 downto 0);
		clock		   :	in  std_logic;
		clear		   :	in  qbit;
		Q 				:	out qbit_vector(40 downto 0);
		adressIn	   :  in  qbit_vector(3 downto 0);
		adressOut   :  out qbit_vector(3 downto 0)
	);
end reg;
----------------------------------------------------
architecture behv of reg is

	signal Q_tmp      : qbit_vector(40 downto 0);
	signal Adress_tmp : qbit_vector(3 downto 0);
begin
	process(I, clock, clear)
	begin

	if clear = 3 then
			-- use 'range in signal assigment 
			Q_tmp <= (Q_tmp'range => 0);
			adress_tmp <=(adress_tmp'range => 0);
	elsif (clock='1' and clock'event) then
		   Q_tmp <= I;
			Adress_tmp <= AdressIn;
	end if;

	end process;

	-- concurrent statement
	adressOut <= adressIn;
	Q <= Q_tmp;

end behv;