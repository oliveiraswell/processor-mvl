library	ieee;
use ieee.std_logic_1164.all;  
use ieee.std_logic_arith.all;			   
use ieee.std_logic_unsigned.all;
use work.constant_lib.all; 
use work.mvl.all;
-------------------------------------------
entity microprocessor is
	port(
		clk_in 	: in std_logic;
		rst_in 	: in qbit
	);
end entity;
-------------------------------------------
architecture structure of microprocessor is
	
	signal clk			:  std_logic;
	signal rst			:	qbit;
	
	signal pipereg1_IN,
			 pipereg2_IN,
			 pipereg3_IN 				    	: qbit_vector(40 downto 0);		  
	signal pipereg1_OUT,
			 pipereg2_OUT,
			 pipereg3_OUT 		     			: qbit_vector(40 downto 0);	
	
	signal instruction_IF,
			 instruction_ID,
			 RegData,
			 ReadData1_ID,
	  	    ReadData2_ID,
			 ReadData1_IE,
			 ReadData2_IE,
			 AluResult_IE,
		    AluResult_WB,
			 MemWriteData_IE,
			 MemWriteData_WB,
			 stack_IF_WB,
			 foward_data_ID,
			 foward_data_IE 				:qbit_vector(7 downto 0);	  
    
    signal MemAddress_ID,
	        MemAddressIn_IE,
			  JumpAddress,
			  Adress_IF_out,
			  Adress_ID_in,
			  Adress_ID_out,
			  Adress_IE_in,
			  Adress_IE_out,
			  Adress_WB_in,
			  Adress_WB_out				:qbit_vector(3 downto 0);
    
    signal RegDest,
			  RegDest_ID,
			  RegDestIn_IE,
     		  RegDestOut_IE,
			  RegDestIn_WB					: qbit_vector(1 downto 0);
    
	 signal AluOp_IE,
			  AluOp_ID						: qbit_vector(2 downto 0);
	 
	 signal AluZ,
			  RegWrite,
			  Branch_ID,
			  BranchIn_IE,
			  RegWrite_ID,
    		  MemWrite_ID,
			  RegWriteIn_IE,
			  MemWriteIn_IE,
			  RegWriteOut_IE,
    		  MemWriteOut_IE,
			  ArithmeticOp_ID,
			  ArithmeticOp_IE,
			  FowardEnable_iD,
			  FowardEnable_iE,
			  FowardOp_ID,
			  FowardOp_IE,
			  RegWriteIn_WB,
			  MemToReg_WB,
			  MemWrite_WB,
    		  Branch_IE,
			  MemToReg_IE,
			  MemWrite_IE,
			  flush,
			  reset,
			  stack_op_IE,
			  stack_op_ID,	
			  stack_op_out_IE,
			  postFlush_IF,
			  postFlush_ID,
			  exception_interruption,
			  exception_interruption_IE,
			  exception_interruption_WB,
			  stack_overflow,
			  context_change,
			  stack_op_buss,
			  stack_op_IW,
			  foward_data_enable_ID,
 			  foward_data_enable_IE : qbit;
			  
begin

	iu: entity work.interruption_unit port map(clk_in,
															 rst_in,
															 exception_interruption,
															 clk,
															 rst,
															 stack_overflow,
															 context_change,
															 Adress_WB_out);

	O3: entity work.extended_OR(extended_or) port map (rst,flush,reset);
		
	U1	: entity work.instruction_fetch port map ( clk,
																	 rst,
																	 AluZ,
																	 JumpAddress,
																	 instruction_IF,
																	 stack_IF_WB, --out
																	 stack_op_buss,
																	 RegData,
																	 postFlush_IF, 
																	 stack_overflow,
																	 Adress_IF_Out,
																	 context_change);  --in
	
	pipereg1_IN(7 downto 0) 		<= instruction_IF;
	pipereg1_IN(8)						<= postFlush_IF;
	
	IF_ID: entity work.reg port map (pipereg1_IN,clk, reset, pipereg1_OUT, Adress_IF_out, Adress_ID_in);

	instruction_ID						<= pipereg1_OUT(7 downto 0);
	postFlush_ID						<= pipereg1_OUT(8);

	U2	: entity work.instruction_decode port map(postFlush_ID,
																	clk,
																	rst,
																	ALuZ,
																	instruction_ID,
																	RegWrite,
																	RegDest,
																	RegData,
																	AluOp_ID,
																	Branch_ID,
																	ReadData1_ID,
																	ReadData2_ID,
																	RegWrite_ID,RegDest_ID,
																	MemWrite_ID,
																	MemAddress_ID,
																	ArithmeticOp_ID,
																	FowardEnable_ID,
																	FowardOp_ID,
																	stack_op_ID,
																	foward_data_ID,
																	foward_data_enable_ID,
																	Adress_ID_in,
																	Adress_ID_out);
	
	pipereg2_IN(1 downto 0)			<= RegDest_ID;
	pipereg2_IN(2)						<= RegWrite_ID;
	pipereg2_IN(6 downto 3)			<= MemAddress_ID;
	pipereg2_IN(7)						<= MemWrite_ID;
	pipereg2_IN(15 downto 8)		<= ReadData1_ID;
	pipereg2_IN(23 downto 16)		<= ReadData2_ID;
	pipereg2_IN(24)					<= Branch_ID;
	pipereg2_IN(27 downto 25)		<= AluOp_ID;
	pipereg2_IN(28)					<= ArithmeticOp_ID;
	pipereg2_IN(29)					<= FowardEnable_ID;
	pipereg2_IN(30)					<= FowardOp_ID;
	pipereg2_In(31)					<= stack_op_ID;
	pipereg2_In(39 downto 32)		<= foward_data_ID;
	pipereg2_In(40)					<= foward_data_enable_ID;
	
	ID_IE: entity work.reg port map(pipereg2_IN,
	                                clk,
											  reset,
											  pipereg2_OUT,
											  Adress_ID_out,
											  Adress_IE_in);
	
	RegDestIn_IE    					<= pipereg2_OUT(1 downto 0);
	RegWriteIn_IE   					<= pipereg2_OUT(2);
	MemAddressIn_IE 					<= pipereg2_OUT(6 downto 3);
	MemWriteIn_IE  					<= pipereg2_OUT(7);
	ReadData1_IE 						<= pipereg2_OUT(15 downto 8);
	ReadData2_IE  						<= pipereg2_OUT(23 downto 16);
	BranchIn_IE    					<= pipereg2_OUT(24);
	AluOp_IE      						<= pipereg2_OUT(27 downto 25);
	ArithmeticOp_IE					<= pipereg2_OUT(28);
	FowardEnable_IE					<= pipereg2_OUT(29);
	FowardOp_IE							<= pipereg2_OUT(30);
	stack_op_IE							<= pipereg2_OUT(31);
	foward_data_IE						<= pipereg2_OUT(39 downto 32);
	foward_data_enable_IE			<= pipereg2_OUT(40);
	
	U3 : entity work.instruction_execution port map( clk,
																	 rst,
																	 AluOp_IE,
							 										 BranchIn_IE,
																	 ReadData1_IE,
																	 ReadData2_IE,
							 										 RegWriteIn_IE,
													   			 RegDestIn_IE,
																	 MemWriteIn_IE,
							 										 MemAddressIn_IE,
																	 AluResult_IE,
																	 AluZ,
								 									 JumpAddress,
							 									 	 RegWriteOut_IE,
								 									 RegDestOut_IE,
								 									 MemToReg_IE,
								 									 MemWriteData_IE,
								 									 MemWriteOut_IE,
								 									 ArithmeticOp_IE,
								 									 FowardEnable_IE,
								 									 FowardOp_IE,
							 										 RegData,
							 										 stack_op_IE,
							 										 stack_op_out_IE,
							 										 foward_data_IE,
								 									 foward_data_enable_IE,
																	 exception_interruption_IE,
																	 Adress_IE_in,
																	 Adress_IE_out);
	
	pipereg3_IN(0)						<= MemToReg_IE;
	pipereg3_IN(8 downto 1)			<= MemWriteData_IE;
	pipereg3_IN(9)						<= MemWriteOut_IE;
	pipereg3_IN(11 downto 10)		<= RegDestOut_IE;
	pipereg3_IN(12)					<= RegWriteOut_IE;
	pipereg3_IN(20 downto 13)		<= AluResult_IE;
	pipereg3_IN(21) 					<= AluZ;
	pipereg3_IN(22)					<= stack_op_out_IE;
	pipereg3_IN(23)					<= exception_interruption_IE;
	
	IE_WB: entity work.reg port map(			 pipereg3_IN,
														 clk,
														 reset,
														 pipereg3_OUT,
														 Adress_IE_out,
														 Adress_WB_in);
	
	MemToReg_WB			 				<= pipereg3_OUT(0);
	MemWriteData_WB 					<= pipereg3_OUT(8 downto 1);
	MemWrite_WB 			 			<= pipereg3_OUT(9);
	RegDestIn_WB 			 			<= pipereg3_OUT(11 downto 10);
	RegWriteIn_WB 			 			<= pipereg3_OUT(12);
	AluResult_WB			 			<= pipereg3_OUT(20 downto 13);
	flush 					 			<= pipereg3_OUT(21) ;
	stack_op_IW 			 			<= pipereg3_OUT(22);
	exception_interruption_WB     <= pipereg3_OUT(23);

	U4   	: entity work.write_back port map (  clk,
															 rst,
									 						 AluResult_WB,
									 						 AluZ,
									 						 RegWriteIn_WB,
									 						 RegDestIn_WB,
								 							 MemToReg_WB,
									 						 MemWriteData_WB,
														 	 MemWrite_WB,
									 						 RegDest,
													 		 RegData,
													 		 stack_op_IW,
													 		 stack_op_buss, -- conecta com IF 
													 		 stack_IF_WB, --stack_in_data
													 		 RegWrite,
															 Adress_WB_in,
															 Adress_WB_out,
															 exception_interruption_WB,
															 exception_interruption);
	
end structure;