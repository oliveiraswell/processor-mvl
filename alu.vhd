library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.constant_lib.all;
use work.mvl.all;

entity alu is
	port (
		num_A			: 	in qbit_vector(7 downto 0);
		num_B			: 	in qbit_vector(7 downto 0);
		jpsign		:	in qbit;
		ALUOperation:	in qbit_vector(1 downto 0);
		ALUz			:	out qbit;
		ALUout		:	out qbit_vector(7 downto 0)
	);
end alu;

architecture behv of alu is

	signal alu_tmp: qbit_vector(7 downto 0); 				-- temporary signal to hold the result of ADD, SUB and ByPass Operations.
	signal mult_tmp: qbit_vector(15 downto 0);			-- temporary signal to hold the result of MULT operation.

begin

	process(num_A, num_B, ALUOperation,mult_tmp)
	begin
		if (ALUOperation  = (0,0)) then
				alu_tmp <= num_A + num_B;						-- ADD
				
		elsif (ALUOperation  = (0,1)) then  
				alu_tmp <= num_A - num_B;						-- SUBT

		elsif (ALUOperation  = (0,2)) then 
				mult_tmp <= num_A * num_B;						-- MULT
				alu_tmp<= mult_tmp(7 downto 0);   			-- take only the last 8 qbits of the result.
		
		elsif (ALUOperation  = (0,3)) then 
				alu_tmp <= num_B;									-- ByPass
		
		elsif (ALUOperation  = (1,0)) then					-- DIV
				alu_tmp <= num_A / num_B;   					-- take only the last 8 qbits of the result.
		
		elsif (ALUOperation  = (1,1)) then
				alu_tmp <= num_A > num_B;	
		
		elsif (ALUOperation  = (1,2)) then
				alu_tmp <= num_A < num_B;
				
		end if;
	end process;
	
	process(jpsign, num_A, alu_tmp)
	begin
		if (jpsign = 3 and (num_A = ZERO & ZERO)) then
			ALUz <= 3;												-- generates flush signals when the operand is Zero and the Instruction is a JUMP.
		else
			ALUz <= 0;
		end if;
	end process;
	ALUout <= alu_tmp;											-- Output assignment.
end behv;