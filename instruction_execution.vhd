----------------------------------------------------
library	ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.constant_lib.all; 
use work.mvl.all;
----------------------------------------------------
entity instruction_execution is 
	port(
		--Clock and instruction
		clk		: in std_logic;
		reset		: in qbit;
		
		AluOp		: in qbit_vector( 2 downto 0);
		BranchIn	: in qbit;
		
		--Registers data
		ReadData1	: in qbit_vector(7 downto 0); 		--Data from Register1
		ReadData2	: in qbit_vector(7 downto 0); 		--Data from Register2

		--Write in register?
		RegWriteIn	: in qbit;					 				--Instruction write in register?
		RegDestIn	: in qbit_vector( 1 downto 0); 		--Register2 that instruction wanna write.
		--Write in memory?
		MemWriteIn	: in qbit;								 	--Instruction write in memory?
		MemAddressIn: in qbit_vector( 3 downto 0);	   --MemAddress that instruction wanna write.

		--Output signals
		AluResult	: out qbit_vector(7 downto 0);
		AluZ			: out qbit;
		JumpAddress	: out qbit_vector(3 downto 0);
		
		--RegInfo Out
		RegWriteOut	: out qbit;
		RegDestOut	: out qbit_vector( 1 downto 0);
		MemToReg 	: out qbit;

		--MemInfo out
		MemWriteData: out qbit_vector(7 downto 0);
		MemWriteOut : out qbit;
		
		ArithmeticOp : in qbit;
		
		FowardEnable   : in qbit;
		FowardOp 		: in qbit; 
		
		RegData			: in qbit_vector(7 downto 0);
		stack_op_in		: in qbit;
		stack_op_out	: out qbit;
		
		foward_data					: in qbit_vector(7 downto 0);
		foward_data_enable		: in qbit;
		exception_interruption	: out qbit;
 	   Adress_in				: in qbit_vector(3 downto 0);
		Adress_out				: out qbit_vector(3 downto 0)
	);
end entity;
----------------------------------------------------
architecture behav of instruction_execution is
	--
	signal ExtendedAddress 	: qbit_vector(7 downto 0);
	signal ExtendedImmediate: qbit_vector(7 downto 0);
	--Alu's input
	signal R1, R2 				: qbit_vector(7 downto 0);
	signal exception_interruption_intern: qbit;
begin
	Adress_out <= Adress_In;
	RegWriteOut <= RegWriteIn;
	RegDestOut <= RegDestIn;
	MemWriteOut <= MemWriteIn;
	JumpAddress <= MemAddressIn;
	ExtendedAddress <= ZERO & MemAddressIn;
	stack_op_out <= stack_op_in;
	
	MemToReg <= 3 when AluOp = (2,0,3)
		else 0;
		
	ExtendedImmediate <= ZERO & MemAddressIn when MemAddressIn(3) = 0
		else ONE & MemAddressIn;
	
	ao: entity work.alu(behv) port map(R1, R2, BranchIn, AluOp(1 downto 0), AluZ, AluResult);
	
	R1 <= foward_data 				when (FowardEnable = 3 and foward_data_enable = 1)
		else RegData   				when (FowardEnable = 3 and FowardOp = 1)
		else ReadData1;

	MemWriteData <= R1;
	
	R2 <= foward_data 				when (FowardEnable = 3 and foward_data_enable = 2)
		else RegData 					when (FowardEnable = 3 and FowardOp = 2)
		else ReadData2 				when (ArithmeticOp = 3)
		else ExtendedImmediate 		when (AluOp = (2,0,0))
		else ExtendedAddress;
	
	exception_interruption <= exception_interruption_intern;
	
	process (clk, reset)
	begin
		if reset = 1 then
			exception_interruption_intern <= 0;
		elsif(clk'event and clk = '0') then
			if(R2 = ZERO & ZERO and AluOp = (1,1,0)) then
				exception_interruption_intern <= 1;
			else 
				exception_interruption_intern <= 0;
			end if;
		end if;
	end process;
end architecture;