library	ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;  
use work.constant_lib.all;
use work.mvl.all;
--------------------------------------------------------
entity PC is
port(	
		clock	                :	in std_logic;
		reset	                :	in qbit;
		PCSrc	                :	in qbit; --1 = Jump, 0 = inc
		PCin						 :	in qbit_vector(3 downto 0);
		PCout						 :	out qbit_vector(3 downto 0);
		context_change_in		 : in qbit
);
end PC;
--------------------------------------------------------
architecture behv of PC is

	signal tmp_PC: qbit_vector(3 downto 0) ;
	signal context_change_PC: qbit_vector (3 downto 0);
	
begin
				
	process(reset,clock,tmp_PC, context_change_in)
	begin
		if reset = 3 then
			tmp_PC <= (0,0,3,3);
		elsif context_change_in = 3 then
			tmp_PC <= ZERO;
		elsif (clock'event and clock = '1') then
			if PCSrc = 3 then	
				tmp_PC <= PCin+1;
			else
				tmp_PC <= tmp_PC + 1;
			end if;
		end if;
 	   PCout <= tmp_PC;
	end process;
end behv;