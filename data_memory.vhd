library	ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;   
use work.constant_lib.all;
use work.mvl.all;
----------------------------------------------------
entity data_memory is
	port (
		clock			: 	in std_logic;
		rst			: 	in qbit;
		Mre			:	in qbit;
		Mwe			:	in qbit;
		address		:	in qbit_vector(3 downto 0);
		data_in		:	in qbit_vector(7 downto 0);
		data_out		:	out qbit_vector(7 downto 0) 
	);
end entity;
----------------------------------------------------
architecture behv of data_memory is			
	type ram_type is array (0 to 255) of qbit_vector(7 downto 0);
	signal tmp_ram: ram_type;
begin							
	write: process( clock,rst, Mre, address, data_in)
	begin
		if rst = 3 then		
			tmp_ram <= (
						0 => (0,0,0,0,0,0,0,0),	   		
						1 => (0,0,0,0,0,0,0,0),			
						2 => (0,0,0,0,0,0,0,0),			
						3 => (0,0,0,0,0,0,0,0),			
						4 => (0,0,0,0,0,0,0,0),			
						5 => (0,0,0,0,0,0,0,0),			
						6 => (0,0,0,0,0,0,0,0),			
						7 => (0,0,0,0,0,0,0,0),			
						8 => (0,0,0,0,0,0,0,0),			
						9 => (3,3,3,3,3,3,3,3),			 			-- this address has the value of -1 just to testing purposes.
						
				 others => (0,0,0,0,0,0,0,0));
		else
			if (clock'event and clock = '1') then				-- Writing takes place in the positive edge of the clock signal.
				if (Mwe = 3 and Mre = 0) then
					tmp_ram(conv_integer(to_std_vector(address))) <= data_in;
				end if;
			end if;
		end if;
	end process;

	read: process(clock,rst, Mwe, address)
	begin
		if rst = 3 then
			data_out <= ZERO & ZERO;
		else
			if (clock'event and clock = '0') then				-- Reading takes place in the negative edge of the clock signal.
				if (Mre =3 and Mwe = 0) then								 
					data_out <= tmp_ram(conv_integer(to_std_vector(address)));
				end if;
			end if;
		end if;
	end process;
end behv;