library	ieee;
use ieee.std_logic_1164.all;  
use ieee.std_logic_arith.all;			   
use ieee.std_logic_unsigned.all;
use work.constant_lib.all;
use work.mvl.all; 
----------------------------------------------------
entity instruction_decode is 
	port (
		postFlush		: in qbit;
		--Clock and instruction
		clk			: in std_logic; 
		reset, int	: in qbit;								-- Reset and int are both reset signal, int takes places when a JUMP occurs
		instruction	: in qbit_vector(7 downto 0); 	--Instruction fetched
		WEnable		: in qbit;								--Signal that enable write in register
		WRegister	: in qbit_vector( 1 downto 0);	--Register that will be writen.
		WData			: in qbit_vector(7 downto 0); 		--Data to write in register

		--Instruction
		AluOp			: out qbit_vector( 2 downto 0);
		Branch		: out qbit;
		--Registers data
		ReadData1Out	: out qbit_vector(7 downto 0); 	--Data from Register1
		ReadData2Out	: out qbit_vector(7 downto 0); 	--Data from Register2

		--Write in register?
		RegWrite	: out qbit;					 				--Instruction write in register?
		RegDest		: out qbit_vector( 1 downto 0); 	--Register1 that instruction wanna write.
		--Write in memory?
		MemWrite	: out qbit;					 				--Instruction write in memory?
		MemAddress	: out qbit_vector( 3 downto 0);  	--MemAddress that instruction wanna write.
		
		ArithmeticOp : out qbit;
		
		FowardEnable   : out qbit;
		FowardOp 		: out qbit;
		
		stack_op					: out qbit;
		
		foward_data				: out qbit_vector(7 downto 0);
		foward_data_enable	: out qbit;
 	   Adress_in				: in qbit_vector(3 downto 0);
		Adress_out				: out qbit_vector(3 downto 0)		
		
	);
end entity;
------------------------------------------------------------------------------------------------
architecture behav of instruction_decode is

	--Registers
	signal RegLastCallIn, RegLastCallOut, RegDest1, RegDest2, Register1, RegDestBuss, Register2, OPCODE : qbit_vector(1 downto 0);
	signal Operation 						   : qbit_vector(2 downto 0);
	signal FowardEnableRegisters, FowardEnableOperators						: qbit;
	signal ReadData1, ReadData2 : qbit_vector(7 downto 0);
	signal NotUseRegDest: qbit;
	signal flushWrite: qbit;
begin
	
	fr : entity work.reg_file(behv) port map (clk,reset, flushWrite, 3, 3, WRegister, Register1, Register2, WData, ReadData1Out, ReadData2Out);
	
	reg_last_instruction : entity work.reg_two(behv) port map (RegLastCallIn, clk, reset, int, RegLastCallOut);
	
	and_last_instruction : entity work.and_3(arch_and) port map (FowardEnableRegisters, FowardEnableOperators, FowardEnable);
	 	
	process (clk,OPCODE, Instruction, int) is
	
	begin
		if int = 3 then						-- if a JUMP occurs then the ID is set to reset.
			operation <= (0,0,0);
			Branch<= 0;
			Register1<= (3,3);
			Register2<= (3,3);
			RegWrite<= 0;
			MemWrite <= 0;
			MemAddress<= (0,0,0,0);
			ArithmeticOp <= 0;
			RegLastCallIn <= (3,3);
			FowardEnableRegisters  <= 0;
			FowardEnableOperators  <= 0;
			stack_op <= 0;
			NotUseRegDest <= 3;
			foward_data_enable <= 0;
			FowardOp <= 0;
			foward_data_enable <= 0;
			RegDest1<=(3,3);
			RegDest2<=(3,3);
			
		else
			FowardOp <= 0;
			ArithmeticOp <= 0;
			foward_data_enable <= 0;
			
		   RegLastCallIn <= RegDestBuss;
			
			Register1 <= instruction(5 downto 4);
			Register2 <= instruction( 3 downto 2);
			--MemAddres signal
			MemAddress <= instruction( 3 downto 0);
			--RegDest
			RegDest1  <= instruction(5 downto 4);
			RegDest2  <= instruction( 1 downto 0);
			
			--Default values
			MemWrite <= 0;
			RegWrite <= 0;

			Branch <= 0;
			
			FowardEnableRegisters  <= 0;
			FowardEnableOperators  <= 3;
			stack_op <= 0;
			NotUseRegDest <= 0;
			
			OPCODE <= instruction(7 downto 6);
			
			if( postFlush = 0)  then 
			
				if(Register1 = RegLastCallOut and  OPCODE /= PUSH) then
					FowardEnableRegisters   <=  3;
					FowardOp 					<=  1;
					
				elsif (Register2 = RegLastCallOut and  OPCODE /= AddI ) then
					FowardEnableRegisters   <=  3;
					FowardOp						<=  2;
				end if;
				
				if(Register1 = WRegister and OPCODE /= PUSH and OPCODE /= NOP and (Register1 /= RegLastCallOut)) then
					FowardEnableRegisters <= 3;
					foward_data_enable <= 1;
					
				elsif(Register2 = WRegister and OPCODE /= AddI and OPCODE /= NOP and (Register2 /= RegLastCallOut)) then
					foward_data_enable <= 2;
					FowardEnableRegisters <= 3;
				end if;
		
			end if;
			
			
			if (OPCODE  = LOAD) then 	
					Operation 					 <= (2,0,3);
					RegWrite 					 <= 3;
					ArithmeticOp 				 <= 0;

			elsif (OPCODE  = STORE) then
					Operation  					 <= (3,0,3);
					MemWrite  					 <= 3;
					ArithmeticOp  				 <= 0;
					NotUseRegDest  			 <= 3;

			elsif (OPCODE  = JUMP) then
					Operation  					 <= (0,0,1); --Dont care
					Branch  					    <= 3;
					NotUseRegDest  			 <= 3;

			elsif (OPCODE  = AddI) then
					Operation 					 <= (2,0,0);
					RegWrite 					 <= 3;
			elsif (OPCODE  = ADD) then
					Operation 					 <= (1,0,0);
					RegWrite 					 <= 3;
					ArithmeticOp 				 <= 3;

			elsif (OPCODE  = SUBT) then
					Operation 					 <= (1,0,1);
					RegWrite 					 <= 3;
					ArithmeticOp 				 <= 3;

			elsif (OPCODE  = MULT) then
					Operation 					 <= (1,0,2);
					RegWrite <= 3;
					ArithmeticOp <= 3;

			elsif (OPCODE  = MOV) then
					Operation  					 <= (1,0,3);
					RegWrite  					 <= 3;
					ArithmeticOp  				 	 <= 3;

			elsif (OPCODE  = PUSH) then
					stack_op 					 <= 1;
					Operation 					 <= (1,0,3);
					ArithmeticOp 				 <= 3; -- r2
					NotUseRegDest  			 <= 3;

			elsif (OPCODE  = POP) then
					stack_op  					 <= 2;
					Operation  					 <= (1,0,3);
					RegWrite  					 <= 3;
					ArithmeticOp  				 <= 3;

			elsif (OPCODE = DIV) then
					Operation  					 <= (1,1,0);
					RegWrite  					 <= 3;
					ArithmeticOp  				 <= 3;

			elsif (OPCODE = GT) then
					Operation  					 <= (1,1,1);
					RegWrite  					 <= 3;
					ArithmeticOp  				 <= 3;
			
			elsif (OPCODE = LS) then
					Operation  					 <= (1,1,2);
					RegWrite  					 <= 3;
					ArithmeticOp  				 <= 3;
					
			else 
					ArithmeticOp  				 	 	 <= 0;
					FowardEnableRegisters  			 <= 0;
					FowardEnableOperators  			 <= 0;
					NotUseRegDest  					 <= 3;
					Operation  					 <= (0,0,0); --Dont care

			end if;
		end if;
		AluOp<= Operation;
	end process;
	
	foward_data <= WData;
	
	Adress_out <= Adress_In;
	
	flushWrite <= WEnable when postFlush = 0 else 0;
	
	RegDestBuss <= (3,3) when NotUseRegDest = 3 
		else RegDest1 when operation(2) = 2
		else RegDest2;
		
	RegDest <= RegDestBuss;
end architecture;