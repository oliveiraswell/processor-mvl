library	ieee;
use ieee.std_logic_1164.all;  
use ieee.std_logic_arith.all;			   
use ieee.std_logic_unsigned.all;
use work.constant_lib.all;
use work.mvl.all; 
--------------------------------------------------
entity write_back is 
	port
	(
		--Clock and instruction
		clk						: in std_logic;
		reset						: in qbit;
		--Alu signals
		AluResult				: in qbit_vector(7 downto 0);
		AluZ						: in qbit;
		
		--RegInfo Out
		RegWriteIn				: in qbit;
		RegDestIn 				: in qbit_vector( 1 downto 0);
		MemToReg 				: in qbit;

		--MemInfo out
		MemWriteData			: in qbit_vector(7 downto 0);
		MemWrite 				: in qbit;

		RegDest 					: out qbit_vector( 1 downto 0);
		RegData 					: out qbit_vector( 7 downto 0);
		stack_op_in				: in qbit;
		stack_op_out			: out qbit;
		stack_in_data			: in qbit_vector( 7 downto 0);
		RegWrite 				: out qbit;
 	   Address_in						: in qbit_vector(3 downto 0);
		Address_out							: out qbit_vector(3 downto 0);
		exception_interruption_in		: in qbit;
		exception_interruption_out		: out qbit
	);
end entity;
--------------------------------------------------
architecture behav of write_back is
	signal Address 	: qbit_vector(3 downto 0);
	signal MemData 	: qbit_vector(7 downto 0);
	signal MemRead		: qbit;
begin
	
	exception_interruption_out <= exception_interruption_in;
	Address_out <= Address_In;
	stack_op_out  <= stack_op_in;
	
	Address  <= AluResult(3 downto 0);

	RegWrite <= RegWriteIn;
	RegDest  <= RegDestIn;

	RegData <= stack_in_data when stack_op_in = 2
		else AluResult when MemToReg = 0
		else MemData;
	
	inv: entity work.inverter(inverter) port map (MemWrite,MemRead);
	
	mem: entity work.data_memory(behv) port map (clk,reset, MemRead, MemWrite, Address, MemWriteData, MemData);
end;