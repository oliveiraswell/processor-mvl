library ieee;
use ieee.std_logic_1164.all;
use work.all;
use work.mvl.all;

-------------------------------------------------

entity Inverter is
	port(
		A: 	in qbit;
		O:	   out qbit
	);
end Inverter;

-------------------------------------------------

architecture inverter of Inverter is
begin
	process(A)
	begin
		-- use case statement
		case A is
			when 0 => O <= 3;
			when 1 => O <= 2;
			when 2 => O <= 1;
			when 3 => O <= 0;	
		end case;
	end process;
end inverter;