library	ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;	
use work.constant_lib.all;
use work.mvl.all;

------------------------
entity instruction_memory is
	port (
		clock							: in  std_logic;
		rst							: in  qbit;
		int 							: in  qbit; 								-- int is a signal that indicates JUMP instruction
		jump_adress					: in  qbit_vector(3 downto 0);
		address						: in  qbit_vector(3 downto 0);
		data_out						: out qbit_vector(7 downto 0);
		stack_in						: in 	qbit_vector(7 downto 0);
		stack_pointer				: in 	qbit_vector(3 downto 0);
		stack_op						: in  qbit;
		stack_out					: out qbit_vector(7 downto 0);
		program_size				: out qbit_vector(3 downto 0)
	);
end entity;

--------------------------

architecture behv of instruction_memory is			



	type ram_type is array (0 to 255) of qbit_vector(7 downto 0);

	--Signal
	signal tmp_ram			: ram_type; 
	signal tmp_address   : qbit_vector(3 downto 0);
	
	signal stack_push_enable, stack_pop_enable : qbit;
begin	
	tmp_address <= jump_adress when int = 3 else address;  -- If JUMP occurs then the address to read is not the one 
	
	stack_push_enable <=  3 when stack_op  = 1 else 0;
	stack_pop_enable  <=  3 when stack_op  = 2 else 0;
	stack_out <= tmp_ram(conv_integer(to_std_vector(stack_pointer-1)));
	
	write: process(clock,rst, address, stack_push_enable)							 -- from PC but the JUMP Address
	begin
		if (clock'event and clock = '1') then 
			if (stack_push_enable = 3) then
						tmp_ram(conv_integer(to_std_vector(stack_pointer))) <= stack_in;
			elsif (stack_pop_enable = 3) then 
						tmp_ram(conv_integer(to_std_vector(stack_pointer-1))) <= ZERO & ZERO;
			end if;
		end if;
	
	if rst = 3 then		
			tmp_ram <= (
			0 => (0,0, 0,0, 0,0, 0,0),
			1 => (3,1, 0,0, 0,0, 0,0),
			2 => (3,1, 0,0, 0,1, 0,0),
			3 => (3,1, 0,0, 0,2, 0,0),
			4 => (3,1, 0,0, 0,3, 0,0),
			5 => (3,1, 0,0, 1,0, 0,0),
			6 => (3,1, 0,0, 1,1, 0,0),
			7 => (3,1, 0,0, 1,2, 0,0),
			8 => (3,1, 0,0, 1,3, 0,0),
			9 => (3,1, 0,0, 2,0, 0,0),
			10 => (3,1, 0,0, 2,1, 0,0),
			15 => (2,0, 2,0, 0,0, 2,0),
			16 => (2,0, 2,1, 0,0, 2,1),
			17 => (2,3, 0,1, 0,0, 2,1),
			18 => (2,0, 0,1, 0,0, 0,2),
			19 => (2,0, 0,2, 0,0, 0,2),
			20 => (1,0, 0,1, 0,2, 0,3),
			21 => (1,3, 0,0, 0,3, 1,0),
			22 => (1,1, 0,3, 1,0, 0,1),
			23 => (2,2, 0,2, 2,0, 0,1),
			24 => (3,0, 0,2, 2,0, 0,1),
			25 => (0,1, 0,1, 0,1, 3,0),
			26 => (0,0, 0,0, 0,0, 0,0),
			27 => (0,0, 0,0, 0,0, 0,0),
			28 => (3,3, 0,2, 0,0, 2,1),
			29 => (3,1, 0,0, 0,2, 0,0),
			30 => (3,2, 0,0, 0,0, 0,3),
			31 => (2,1, 0,2, 0,3, 1,1),
			32 => (1,1, 0,1, 1,1, 1,2),
			33 => (0,1, 1,2, 0,1, 0,1),
			others => (0,0,0,0,0,0,0,0));
                   program_size <= (0,2,0,3);
			end if;																
			
	end process;

	read: process( clock,rst,tmp_address, stack_pop_enable)
	begin
		if rst = 3 then
			data_out <= ZERO & ZERO;
		elsif (clock'event and clock = '1') then
			data_out <= tmp_ram(conv_integer(to_std_vector(tmp_address)));				
		end if;
		
	end process;

end behv;