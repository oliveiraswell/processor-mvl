library	ieee;
use ieee.std_logic_1164.all;	
use ieee.std_logic_arith.all;				 
use ieee.std_logic_unsigned.all;	 
use work.constant_lib.all;
use work.mvl.all;
---------------------------------------------------------
entity reg_file is
	port (
		clock	: 	in std_logic; 	
		rst	: 	in qbit;
		RFwe	: 	in qbit;
		RFr1e	: 	in qbit;
		RFr2e	: 	in qbit;	
		RFwa	: 	in qbit_vector(1 downto 0);	
		RFr1a	: 	in qbit_vector(1 downto 0);
		RFr2a	: 	in qbit_vector(1 downto 0);
		RFw	: 	in qbit_vector(7 downto 0);
		RFr1	: 	out qbit_vector(7 downto 0);
		RFr2	:	out qbit_vector(7 downto 0)
	);
end reg_file;
---------------------------------------------------------
architecture behv of reg_file is			

	type rf_type is array (0 to 15) of qbit_vector(7 downto 0);
	signal tmp_rf: rf_type;

begin

	write: process(clock,rst, RFwa, RFwe, RFw)
	begin
		if rst = 3 then				
			tmp_rf <= (tmp_rf'range => ZERO & ZERO);
		else
			if (clock'event and clock = '1') then
			-- Writing takes place in the positive edge of the clock signal.
				if RFwe = 3 then
					tmp_rf(conv_integer(to_std_vector(RFwa))) <= RFw;
				end if;
			end if;
		end if;
	end process;				 
	
	read1: process(clock,rst, RFr1e, RFr1a)
	begin
		if rst= 3 then
			RFr1 <= ZERO & ZERO;
		else
			if (clock'event and clock = '0') then
				if RFr1e = 3 then								 
					RFr1 <= tmp_rf(conv_integer(to_std_vector(RFr1a)));
				end if;
			end if;
		end if;
	end process;
	
	read2: process(clock,rst, RFr2e, RFr2a)
	begin
		if rst = 3 then
			RFr2<= ZERO & ZERO;
		else
			if (clock'event and clock = '0') then							-- Reading takes place in the negative edge of the clock signal.
				if RFr2e = 3 then								 
					RFr2 <= tmp_rf(conv_integer(to_std_vector(RFr2a)));
				end if;
			end if;
		end if;
	end process;	
end behv;