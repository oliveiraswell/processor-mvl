LIBRARY ieee;
USE ieee.std_logic_1164.all;
use work.mvl.all;
 
 PACKAGE constant_lib IS 
	CONSTANT ZERO		: qbit_vector(3 downto 0) 		:= (0,0,0,0);
	CONSTANT ONE 		: qbit_vector(3 downto 0) 		:= (3,3,3,3);
	CONSTANT NOP 		: qbit_vector(1 downto 0) 		:= (0,0);
	CONSTANT JUMP		: qbit_vector(1 downto 0) 		:= (0,1);
	CONSTANT ADD 		: qbit_vector(1 downto 0) 		:= (1,0);
	CONSTANT SUBT 		: qbit_vector(1 downto 0) 		:= (1,1);
	CONSTANT MULT 		: qbit_vector(1 downto 0) 		:= (1,2);
	CONSTANT MOV 		: qbit_vector(1 downto 0) 		:= (1,3);
	CONSTANT ADDI  		: qbit_vector(1 downto 0) 		:= (2,0);
	CONSTANT DIV	  	: qbit_vector(1 downto 0) 		:= (2,1);
	CONSTANT GT	  	: qbit_vector(1 downto 0) 		:= (2,2);
	CONSTANT LOAD 		: qbit_vector(1 downto 0) 		:= (2,3);
	CONSTANT LS	  	: qbit_vector(1 downto 0) 		:= (3,0);
	CONSTANT PUSH	 	: qbit_vector(1 downto 0) 		:= (3,1);
	CONSTANT POP		: qbit_vector(1 downto 0) 		:= (3,2);
	CONSTANT STORE 		: qbit_vector(1 downto 0) 		:= (3,3);
 END constant_lib;