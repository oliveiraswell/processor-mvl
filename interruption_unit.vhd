library	ieee;
use ieee.std_logic_1164.all;  
use ieee.std_logic_arith.all;			   
use ieee.std_logic_unsigned.all;
use work.constant_lib.all;
use work.mvl.all;
--------------------------------------------------
entity interruption_unit is 
	port (
		clk_in						: in std_logic; 
		reset_in						: in qbit;
		arithmetic_exception		: in qbit;
		clk_out						: out std_logic; 
		reset_out					: out qbit;
		stack_overflow				: in qbit;
		context_change				: out qbit;
		address						: in qbit_vector(3 downto 0)
	);
end entity;
---------------------------------------------------
architecture behav of interruption_unit is
	
	signal stop, break: std_logic;
	signal context_change_tmp : qbit;
	signal context_change_activated: qbit;
	signal exception_address : qbit_vector(3 downto 0);
begin
			
	process(arithmetic_exception, reset_in, clk_in)
	begin
			if( reset_in = 3) then
				context_change_tmp <= 0;
				context_change_activated <= 0;
				exception_address <= ZERO;
				break <= 'L';
			elsif((arithmetic_exception = 1 or stack_overflow = 3 ) and context_change_activated /= 1) then
				context_change_tmp <= 3;
				context_change_activated <= 1;
				exception_address <= address;
			elsif stack_overflow = 2 then
				break <= 'H';
			else
				context_change_tmp <= 0;
			end if;
	end process;
	
	process(context_change_tmp, reset_in, clk_in)
	begin
			if(context_change_activated = 1 and (address = (0,0,3,3))) then
				stop <= 'H';
			elsif(reset_in = 3) then
				stop <= 'L';
			end if;
	end process;
	
	reset_out <= reset_in;
	
	context_change <= context_change_tmp;
	
	clk_out   <= stop or clk_in or break;
	
end architecture;