library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_bit.all;
--------------------------------------------------------
package mvl is
	type qbit is range 0 to 3;
	type qbit_vector is array(integer range<>) of qbit;
--------------------------------------------------------
	--Auxiliar functions
	function max (LEFT, RIGHT: INTEGER) return INTEGER;

	--Arithmatical operators - qbit
	function "+" (l, r: qbit) return qbit;
	function carryOut (l,r,c: qbit) return qbit;

	--Arithmatical Oparators - qbit_vector
	function "+" (l, r: qbit_vector) return qbit_vector;
	function "+" (l: qbit_vector; r: natural) return qbit_vector;
	function "-" (l: qbit_vector; r: natural) return qbit_vector;
	function "-" (l, r: qbit_vector) return qbit_vector;
	function "*" (l, r: qbit_vector) return qbit_vector;
	function "/" (l, r: qbit_vector) return qbit_vector;
	function "mod" (l, r: qbit_vector) return qbit_vector;
	function ">" (l, r: qbit_vector) return qbit_vector;
	function "<" (l, r: qbit_vector) return qbit_vector;
	
	--Cast functions
	function to_bit_vector(q : qbit_vector) return bit_vector;
	function to_std_vector(q : qbit_vector) return std_logic_vector;
	function to_qbit_vector(q : bit_vector) return qbit_vector;
	--function to_qbit(q: bit_vector)  return std_logic;

end package mvl;

package body mvl is
	type qbit_table is array(qbit, qbit) of qbit;
	type qbit_arith_table is array (qbit, qbit, qbit) of qbit;
	
	--constant and_table : qbit_table
	
	--First index is CarryIn value.
	constant sum_table : qbit_table := (
		(0, 1, 2, 3),
		(1, 2, 3, 0),
		(2, 3, 0, 1),
		(3, 0, 1, 2)
	);
	
	constant carryOut_table : qbit_arith_table := 
	(
		--Carry in 0
		(
			(0, 0, 0, 0),
			(0, 0, 0, 1),
			(0, 0, 1, 1),
			(0, 1, 1, 1)
		),
		--Carry in 1
		(
			(0, 0, 0, 1),
			(0, 0, 1, 1),
			(0, 1, 1, 1),
			(1, 1, 1, 1)
		),
		--Carry in 2
		(
			(0, 0, 1, 1),
			(0, 1, 1, 1),
			(1, 1, 1, 1),
			(1, 1, 1, 2)
		),
		--Carry in 3
		(
			(0, 1, 1, 1),
			(1, 1, 1, 1),
			(1, 1, 1, 2),
			(1, 1, 2, 2)
		)
	);
	
	
	
	
	--functions body
	
	--Max
	function max (LEFT, RIGHT: INTEGER) return INTEGER is
	begin
		if LEFT > RIGHT then return LEFT;
		else return RIGHT;
		end if;
	end max;
	
	--Cast qbit_vector to bit_vector
	function to_bit_vector(q : qbit_vector) return bit_vector is
		constant size	: natural := q'length;
		constant rsize	: natural := (q'length *2);
		variable result: bit_vector(rsize-1 downto 0);
	begin
		for i in 0 to size-1 loop
			if (q(i) = 3) then
				result(2*i) 	:= '1';
				result(2*i + 1):= '1';
			elsif (q(i) = 2) then
				result(2*i) 	:= '0';
				result(2*i + 1):= '1';
			elsif (q(i) = 1) then
				result(2*i) 	:= '1';
				result(2*i + 1):= '0';			
			else
				result(2*i) 	:= '0';
				result(2*i + 1):= '0';			
			end if;
		end loop;
		return result;
	end;
	
	--Cast qbit_vector to std_logic_vector
	function to_std_vector(q : qbit_vector) return std_logic_vector is
		constant size	: natural := q'length;
		constant rsize	: natural := (q'length *2);
		variable result: std_logic_vector(rsize-1 downto 0);
	begin
		for i in 0 to size-1 loop
			if (q(i) = 3) then
				result(2*i) 	:= '1';
				result(2*i + 1):= '1';
			elsif (q(i) = 2) then
				result(2*i) 	:= '0';
				result(2*i + 1):= '1';
			elsif (q(i) = 1) then
				result(2*i) 	:= '1';
				result(2*i + 1):= '0';			
			else
				result(2*i) 	:= '0';
				result(2*i + 1):= '0';			
			end if;
		end loop;
		return result;
	end;
	
	--Cast bit_vector to qbit_vector
	function to_qbit_vector(q : bit_vector) return qbit_vector is
		constant size	: natural := q'length;
		constant rsize	: natural := (q'length + 1)/2;
		variable result: qbit_vector(rsize-1 downto 0);
	begin
		for i in 0 to rsize-1 loop
			if (q(2*i+1 downto 2*i) = "11") then
				result(i) := 3;
			elsif (q(2*i+1 downto 2*i) = "10") then
				result(i) := 2;
			elsif (q(2*i+1 downto 2*i) = "01") then
				result(i) := 1;		
			else
				result(i) := 0;		
			end if;
		end loop;
		return result;
	end;
	
	--Arithmatical: add qbit
	function "+" (l,r : qbit) return qbit is
	begin
		return (sum_table(l,r));
	end;
	
	function carryOut (l,r,c: qbit) return qbit is
	begin
		return(carryOut_table(c,l,r));
	end carryOut;
	
	--Arithmatical: add qbit_vector
	function "+" (l, r: qbit_vector) return qbit_vector is
		constant size		: natural := max(l'length, r'length);
		variable l_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(l);
		variable r_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(r);
		variable result_bv: bit_vector(size*2-1 downto 0);
		--variable result	: qbit_vector(size-1 downto 0);
	begin
		result_bv := bit_vector(signed(l_bv) + signed(r_bv));
		return to_qbit_vector(result_bv);
	end "+";
	
	--Arithmatical: add qbit_vector
	function "+" (l: qbit_vector; r: natural) return qbit_vector is
		constant size		: natural := l'length;
		variable l_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(l);
		variable result_bv: bit_vector(size*2-1 downto 0);
		--variable result	: qbit_vector(size-1 downto 0);
	begin
		result_bv := bit_vector(signed(l_bv) + r);
		return to_qbit_vector(result_bv);
	end "+";

		--Arithmatical: sub qbit_vector
	function "-" (l: qbit_vector; r: natural) return qbit_vector is
		constant size		: natural := l'length;
		variable l_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(l);
		variable result_bv: bit_vector(size*2-1 downto 0);
		--variable result	: qbit_vector(size-1 downto 0);
	begin
		result_bv := bit_vector(signed(l_bv) - r);
		return to_qbit_vector(result_bv);
	end "-";
	

	
	----Arithmatical: sub qbit_vector
	function "-" (l, r: qbit_vector) return qbit_vector is
		constant size		: natural := max(l'length, r'length);
		variable l_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(l);
		variable r_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(r);
		variable result_bv: bit_vector(size*2-1 downto 0);
		--variable result	: qbit_vector(size-1 downto 0);
	begin
		result_bv := bit_vector(signed(l_bv) - signed(r_bv));
		return to_qbit_vector(result_bv);
	end "-";
	
	
	----Arithmatical: mult qbit_vector
	function "*" (l, r: qbit_vector) return qbit_vector is
		constant size		: natural := max(l'length, r'length);
		variable l_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(l);
		variable r_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(r);
		variable result_bv: bit_vector(size*4-1 downto 0);
		--variable result	: qbit_vector(size-1 downto 0);
	begin
		result_bv := bit_vector(signed(l_bv) * signed(r_bv));
		return to_qbit_vector(result_bv);
	end "*";
	
	----Arithmatical: div qbit_vector
	function "/" (l, r: qbit_vector) return qbit_vector is
		constant size		: natural := max(l'length, r'length);
		variable l_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(l);
		variable r_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(r);
		variable result_bv: bit_vector(size*2-1 downto 0);
		--variable result	: qbit_vector(size-1 downto 0);
	begin
		result_bv := bit_vector(signed(l_bv) / signed(r_bv));
		return to_qbit_vector(result_bv);
	end "/";

		--Arithmatical: greater qbit_vector
	function ">" (l, r: qbit_vector) return qbit_vector is
		constant size		: natural := max(l'length, r'length);
		variable l_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(l);
		variable r_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(r);
		variable result   : bit_vector(size*2-1 downto 0) := to_bit_vector(r);
	begin
		if to_integer(unsigned(l_bv)) > to_integer(unsigned(r_bv)) then
			result := bit_vector(to_unsigned(3,size*2));
			return to_qbit_vector(result);
		else
			result := bit_vector(to_unsigned(0,size*2));
			return to_qbit_vector(result);
		end if;
	end ">";
	
	
	function "<" (l, r: qbit_vector) return qbit_vector is
		constant size		: natural := max(l'length, r'length);
		variable l_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(l);
		variable r_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(r);
		variable result   : bit_vector(size*2-1 downto 0) := to_bit_vector(r);
	begin
		if to_integer(unsigned(l_bv)) < to_integer(unsigned(r_bv)) then
			result := bit_vector(to_unsigned(3,size*2));
			return to_qbit_vector(result);
		else
			result := bit_vector(to_unsigned(0,size*2));
			return to_qbit_vector(result);
		end if;
	end "<";
	
	function "mod" (l, r: qbit_vector) return qbit_vector is
		constant size		: natural := max(l'length, r'length);
		variable l_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(l);
		variable r_bv		: bit_vector(size*2-1 downto 0) := to_bit_vector(r);
		variable result_bv: bit_vector(size*2-1 downto 0);
		--variable result	: qbit_vector(size-1 downto 0);
	begin
		result_bv := bit_vector(signed(l_bv) mod signed(r_bv));
		return to_qbit_vector(result_bv);
	end "mod";
	
	
end package body mvl;

