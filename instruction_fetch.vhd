library	ieee;
use ieee.std_logic_1164.all;  
use ieee.std_logic_arith.all;			   
use ieee.std_logic_unsigned.all;
use work.constant_lib.all;
use work.mvl.all;
--------------------------------------------------
entity instruction_fetch is 
	port (
		--Clock and instruction
		clk		: in std_logic; 
		reset		: in qbit;
		PCSrc		: in qbit; 
		PCin		: in qbit_vector( 3 downto 0);

		instruction : out qbit_vector(7 downto 0);
		
		stack_out					: out qbit_vector(7 downto 0);
		stack_op						: in qbit;
		stack_in 					: in qbit_vector(7 downto 0);
		postFlush					: out qbit;
		stack_overflow 			: out qbit;
		adressOut					: out qbit_vector( 3 downto 0);
		context_change_in			: in qbit
	);
end entity;
---------------------------------------------------
architecture behav of instruction_fetch is
	signal address, stackAdressOut	 : qbit_vector( 3 downto 0);
	signal program_size               : qbit_vector(3 downto 0);
begin
	adressOut <= address;

	pc : entity work.PC(behv) port map (clk, reset, PCSrc, PCin, address, context_change_in);

	mem : entity work.instruction_memory(behv)  port map (clk,reset, PCSrc,PCin, address, instruction, stack_in, stackAdressOut, stack_op, stack_out, program_size);
	
	stack: entity work.stack_pointer(behv) port map(clk, reset, stack_op, stackAdressOut, program_size, stack_overflow);
	
	process(clk, PCSrc)
	begin
			if (clk'event and clk = '1') then
				if PCSrc = 3 then
					postFlush <= 3;
				else
					postFlush <= 0;
				end if;
			end if;
	end process;		
end architecture;