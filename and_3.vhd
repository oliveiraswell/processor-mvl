library ieee;
use ieee.std_logic_1164.all;
use work.all;
use work.mvl.all;
-------------------------------------------------
entity and_3 is
	port(
		A 	: in qbit;
		B 	: in qbit;
		O 	: out qbit
	);
end and_3;
-------------------------------------------------
architecture arch_and of and_3 is
begin
	process(A,B)
	begin
		case A is
			when 3 =>
				case B is
					when 3 => O <= 3;								-- That is, the result is Zero iff A=B=0
					when others => O <= 0;						-- Else, the result is 3.
				end case;
			when others => O <= 0;
		end case;
	end process;
end arch_and;