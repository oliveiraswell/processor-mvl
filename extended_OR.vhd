library ieee;
use ieee.std_logic_1164.all;
use work.all;
use work.mvl.all;
-------------------------------------------------
entity extended_OR is
	port(
		A 	: in qbit; 
		B 	: in qbit;
		O 	: out qbit
	);
end extended_OR;
-------------------------------------------------
architecture extended_or of extended_OR is
begin
	process(A,B)
	begin
		case A is
			when 0 =>
				case B is
					when 0 => O <= 0;								-- That is, the result is Zero iff A=B=0
					when others => O <= 3;						-- Else, the result is 3.
				end case;
			when others => O <= 3;
		end case;
	end process;
end extended_or;