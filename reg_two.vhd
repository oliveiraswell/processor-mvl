library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.mvl.all;
---------------------------------------------------
entity reg_two is

	port(	
		I 		:	in qbit_vector(1 downto 0);
		clock	:	in std_logic;
		clear	:	in qbit;
		int   :	in qbit;
		Q 		:	out qbit_vector(1 downto 0)
	);
end reg_two;
----------------------------------------------------
architecture behv of reg_two is
	signal Q_tmp: qbit_vector(1 downto 0);
begin
	process(I, clock, int, clear)
	begin

	if clear = 3 or int = 3 then
			-- use 'range in signal assigment 
			Q_tmp <= (3,3);
	elsif (clock='1' and clock'event) then
		Q_tmp <= I;
	end if;

	end process;

	-- concurrent statement
	Q <= Q_tmp;

end behv;