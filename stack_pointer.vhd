library	ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;  
use work.constant_lib.all;
use work.mvl.all;

--------------------------------------------------------
entity stack_pointer is
port(	
		clock			:	in std_logic;
		reset			:	in qbit;
		Op				:	in qbit; --1 = Jump, 0 = inc
		PointerOut  :	out qbit_vector(3 downto 0);
		program_size : in qbit_vector(3 downto 0);
		stack_overflow :	out qbit
);
end stack_pointer;
--------------------------------------------------------

architecture behv of stack_pointer is

	signal tmp_pointer: qbit_vector(3 downto 0) ;
	signal stack_overflow_tmp, underflow: qbit;
	
begin
				
	process(reset,clock,Op)
	begin
		if reset = 3 then
			tmp_pointer <= program_size;
		elsif (clock'event and clock = '1') then
			if Op = 1 then	
				  tmp_pointer <= tmp_pointer + 1;
			elsif Op = 2 then 
				tmp_pointer <= tmp_pointer - 1;
			end if;
		end if;
	end process;
	
	process(reset,clock,tmp_pointer)
	begin
		underflow <= 0;
		if stack_overflow_tmp /= 3 and stack_overflow_tmp /= 1 and reset /= 3 then 
			if tmp_pointer < program_size then
				stack_overflow_tmp <= 3;
				underflow <= 3;
			elsif tmp_pointer > (3,3,3,3) then 
				stack_overflow_tmp <= 2;
			else 
				stack_overflow_tmp <= 0;
			end if;
		elsif stack_overflow_tmp = 3 or stack_overflow_tmp = 1 then
			stack_overflow_tmp <= 1;
		else
			stack_overflow_tmp <= 0;
		end if;
	end process;
	
	PointerOut <= tmp_pointer when underflow /= 3 else program_size;
	stack_overflow <= stack_overflow_tmp;
end behv;

